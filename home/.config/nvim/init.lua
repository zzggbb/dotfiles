vim.cmd.syntax 'enable'
vim.cmd.filetype 'on'
vim.cmd.filetype 'indent on'
vim.cmd.filetype 'plugin on'

vim.opt.softtabstop = 2
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.autoindent = true
vim.opt.breakindent = true
vim.opt.encoding = 'utf-8'
vim.opt.number = true
vim.opt.ruler = true
vim.opt.cursorline = true
vim.opt.incsearch = true
vim.opt.wildmenu = true
vim.opt.backspace = 'indent,eol,start'
vim.opt.autoread = true
vim.opt.scrolloff = 3
vim.opt.mouse = 'a'
vim.opt.hlsearch = true
vim.opt.foldmethod = 'indent'
vim.opt.foldignore = ''
vim.opt.swapfile = false
vim.opt.undofile = true
vim.opt.clipboard = 'unnamedplus'

vim.api.nvim_create_autocmd("FileType", {
  pattern = "python",
  callback = function()
    vim.opt.tabstop = 2
    vim.opt.shiftwidth = 2
    vim.opt.softtabstop = 2
    vim.opt.expandtab = true
    vim.opt.autoindent = true
  end,
})

vim.api.nvim_create_autocmd("FileType", {
  pattern = "csv",
  callback = function() vim.opt.wrap = false end
})

vim.api.nvim_create_autocmd({ "BufWritePre" }, {
  pattern = { "*" },
  command = [[%s/\s\+$//e]],
})

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate"
  },
  {
    'maxmx03/solarized.nvim',
    lazy = false,
    priority = 1000,
    config = function()
      vim.o.background = 'dark' -- or 'light'
      vim.cmd.colorscheme 'solarized'
    end,
  },
})

require("nvim-treesitter.configs").setup({
  highlight = {
    enable = true,
    disable = {"python"},
  },
})
