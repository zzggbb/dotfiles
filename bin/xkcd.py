#!/bin/env python3

import requests   
import argparse
import shutil
import sys

API_URL = "http://xkcd.com/{}/info.0.json"

arg_tree = {
    'action': {
        'names': ['action'],
        'other': {
            'help': "The action to be performed",
            'choices': ['update','init']
        }
    },
    'path': {
        'names': ['-p','--path'],
        'other': {
            'help': """
                The path at which this script will perform its actions.
                Defaults to the current directory ("./")
                """,
            'default': './'
        }
    },
    'format': {
        'names': ['-f','--format'],
        'other': {
            'help': """
                The filename format used by comics. 
                Defaults to "{num} - {safe_title}.png"
                """,
            'default': "{num}.png"  
        }
    },
    'verbose': {
        'names': ['-v','--verbose'],
        'other': {
            'help': "enable verbose output",
            'action': 'store_true'
        }
    }
}

def log(string, verbose):
    if verbose:
        print(string, end='\r')
        print(end='\r')

def write_image(url, path):
    response = requests.get(url, stream=True)
    with open(path, 'wb') as f:
        shutil.copyfileobj(response.raw, f)
    del response

def write_current(path, current):
    """
    the id in the file at `path` will be used to keep track of the 
    id of the most current comic that has been saved.
    """
    current_file = "{}/{}".format(path,"current.id")
    with open(current_file, 'w') as f:
        f.write(current)

def read_current(path):
    current_file = "{}/{}".format(path,"current.id")
    try: 
        with open(current_file, 'r') as f:
            return int(f.read())
    except FileNotFoundError:
        return 0

def json(num=None):
    """
    returns a dict that contains all meta-data of the given comic
    if no argument is given, the most recent comic is used
    """
    return requests.get(API_URL.format(num or "")).json()

def update(path, format, verbose): 
    current = read_current(path)
    newest = json()['num'] 
    begin = current + 1
    total = newest - current 
    if total <= 0:
        print("Up to date")
        return
    message = "downloading {} total images, from {} to {}"
    print(message.format(total, begin, newest))
    for i in range(begin,newest+1):
        try:
            data = json(i)
            filename = format.format(**data)
            full_path = '{}/{}'.format(path,filename) 
            write_image(data['img'], full_path)
            write_current(path,str(i))
            message = "downloading {}".format(filename)
            log(message, verbose)
            
        except ValueError:
            # there is no image number 404 
            pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    for k,v in arg_tree.items():
        # feed the arg_tree into parser.add_arguments()
        parser.add_argument(*v['names'], **v['other'])

    args = parser.parse_args()
    if args.action == 'update':
        update(args.path, args.format, args.verbose)
