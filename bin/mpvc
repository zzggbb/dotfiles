#!/bin/sh
# wildefyr - 2016 (c) MIT
# control mpv remotely using JSON ipc

SOCKET=${SOCKET:-/tmp/mpvsocket}
SOCKETCOMMAND="socat - $SOCKET" 
usage() {
    cat << EOF
usage: $(basename $0) [-S socket] [-k] [-a "filenames"] "[-f "format string"] [-qistvVrRxXpmlLh]
    -S : Set socket location [default: $SOCKET].
    -q : Produce no textual output.
    -f : Enter a format string (see Formatting). This cannot be combined with another option.
    -a : Add filenames to current, or new, playlist. This cannot be combined with another option.
    -i : Print all filenames of tracks in current playlist.
    -s : Increase/decrease time in seconds.
    -t : Set absolute time in seconds.
    -r : Go forwards/backwards through the playlist queue.
    -R : Jump to playlist item number.
    -z : Increase/decrease speed relatively to the current speed.
    -Z : Set absolute speed.
    -p : Toggle play/paused.
    -l : Loop currently playing file.
    -L : Loop currently playing playlist.
    -k : Kill the current mpv process controlling the named socket.
    -h : Print this help.
EOF
    test -z $1 && exit 0 || exit $1
}

listFormat(){
    for format in %filename% %year% %genre% %title% \
    %album% %artist% %albumartist% %time% %length% \
    %percentage% %status% %position% %playlist% \
    %speed% %repeat% %single% %frame%; do echo $format; done
}

getMediaFilename() {
    echo "{ \"command\": [\"get_property\",\"media-title\" ] }" | $SOCKETCOMMAND 2> /dev/null | jq -r '.data'
}

getMetadata() {
    test -z $1 && return
    property=$1

    metadata=$(echo "{ \"command\": [\"get_property\", \
        \"metadata/by-key/${property}\" ] }" | $SOCKETCOMMAND 2> /dev/null)
    errorCheck=$(echo "$metadata" | cut -d\" -f 2)

    test "$errorCheck" = "error" && {
        result="N/A"
    } || {
        metadata=$(echo "$metadata" | cut -d: -f 2 | \
        cut -d, -f 1 | sed s/\"//g)
        result="$metadata"
    }

    # test if title has no property
    test "$property" = "title" && test "$result" = "N/A" && {
        result=$(getMediaFilename)
    }

    printf "$result"
}

getDuration() {
    echo "{ \"command\": [\"get_property\", \"duration\"] }" | $SOCKETCOMMAND 2> /dev/null | jq -r '.data'
}

getPercentage() {
    echo "{ \"command\": [\"get_property\", \"percent-pos\" ] }" | $SOCKETCOMMAND 2> /dev/null | jq -r '.data'
}

getValue() {
    property=$1
    value=$(echo "{ \"command\": [\"get_property_string\", \"$property\"] }" | $SOCKETCOMMAND 2> /dev/null)
    value=$(echo $value | cut -d\" -f 4 | cut -d. -f 1)
    test "$value" = "error" && printf "N/A" || printf "$value"
}

# print all filenames in current playlist
getPlaylist() {
    tracks=$(getValue playlist-count)
    for i in $(seq $tracks); do
        playlistCur=$(echo "{ \"command\": [\"get_property_string\", \
            \"playlist/$((i - 1))/filename\"] }" | $SOCKETCOMMAND 2> /dev/null)
        playlistCur=$(printf '%s ' $playlistCur | cut -d\" -f 4)
        currentTrack=$(getValue playlist-pos)
        test "$currentTrack" -eq $((i - 1)) && PRE="- " || PRE="  "
        echo ${PRE}${playlistCur}
    done

}

getSpeed() {
    speed=$(echo "{ \"command\": [\"get_property_string\", \"speed\"] }" | $SOCKETCOMMAND 2> /dev/null)
    speed=$(echo $speed | cut -d\" -f 4)
    speed=$(printf '%0.2f\n' "$speed")

    printf "$speed"
}

getPauseStatus() {
    status=$(echo "{ \"command\": [\"get_property\", \"pause\" ] }" | $SOCKETCOMMAND 2> /dev/null | jq -r '.data')

    test ! -z "$status" && {
        test "$status" = "true" && {
            status="paused"
        } || {
            status="playing"
        }
    } || {
        status="N/A"
    }

    printf "$status"
}

getLoopStatus() {
    property=$1
    loop=$(echo "{ \"command\": [\"get_property\", \
        \"$property\" ] }" | $SOCKETCOMMAND 2> /dev/null)
    loop=$(echo "$loop" | cut -d: -f 2 | cut -d, -f 1 | sed s/\"//g)

    printf "$loop"
}

getFrameNumber() {
    frame=$(echo "{ \"command\": [\"get_property\", \"estimated-frame-number\" ] }" | $SOCKETCOMMAND 2> /dev/null)
    errorCheck=$(echo "$frame" | cut -d\" -f 2)
    echo $errorCheck
    frame=$(echo "$frame" | cut -d: -f 2 | cut -d, -f 1 | sed s/\"//g)

    test "$errorCheck" = "error" && printf "N/A" || printf "$frame"
}

    # Control Functions
###############################################################################

praseAppendTracks() {
    # find media extensions and append a newline to them
    APPENDSTRING=$(echo $APPENDSTRING | sed "
    s#.mp3 #.mp3\n#g
    s#.m4a #.m4a\n#g
    s#.aac #.acc\n#g
    s#.flac #.flac\n#g
    s#.mp4 #.mp4\n#g
    s#.mkv #.mkv\n#g
    s#.flv #.flv\n#g
    s#.ogv #.ogv\n#g
    s#.webm #.webm\n#g
    s#.gif #.gif\n#g
    s#.png #.png\n#g
    s#.jpg #.jpg\n#g
    s#.jpeg #.jpeg\n#g
    ")

    if [ ! -z "$APPENDSTRING" ]; then
        totalFiles=$(printf "${APPENDSTRING}\n" | wc -l)
        for i in $(seq $totalFiles); do
            appendTrack $(printf "${APPENDSTRING}" | sed "$i!d")
            # need slight pause to start mpv
            sleep 0.1
        done
    else
        echo "No files have been passed to -a."
    fi
}

appendTrack() {
    filename=$(printf '%s ' "${@}" )

    test "$QUIETFLAG" != "true" && {
        echo "Adding: $filename"
    }

    pgrep -x "mpv" 2>&1 > /dev/null && {
        echo "{ \"command\": [\"loadfile\", \"$filename\", \"append\" ] }" | $SOCKETCOMMAND > /dev/null
    } || {
        exec mpv --idle --really-quiet --no-audio-display --input-unix-socket=${SOCKET} "$filename" &
    }
}

setTimeRelative() {
    intCheck $1

    time=$(getValue playback-time)
    time=$((time + $1))

    echo "{ \"command\": [\"set_property\", \"playback-time\", $time ] }" | $SOCKETCOMMAND > /dev/null
}

setTimeAbsolute() {
    intCheck $1
    time=$1

    echo "{ \"command\": [\"set_property\", \"playback-time\", $time ] }" | $SOCKETCOMMAND > /dev/null
}

setTrackRelative() {
    intCheck $1

    seconds=$(getValue playback-time)
    currentTrack=$(getValue playlist-pos)
    desiredTrack=$((currentTrack + $1))
    trackCount=$(getValue playlist-count)

    test $desiredTrack -lt 0 && \
        desiredTrack=0
    test $desiredTrack -ge $trackCount && \
        killProcess

    test "$desiredTrack" -lt "$currentTrack" && \
        test "$seconds" -ge 10 && \
        setTimeAbsolute 0 || \
        echo "{ \"command\": [\"set_property\", \
        \"playlist-pos\", $desiredTrack ] }" | $SOCKETCOMMAND > /dev/null
}

setTrackAbsolute() {
    intCheck $1

    currentTrack=$1
    currentTrack=$((currentTrack - 1))
    trackCount=$(getValue playlist-count)

    test $currentTrack -lt 0 || test $currentTrack -ge $trackCount && {
        echo "Item $currentTrack is out of range of playlist." >&2
        exit 1
    }

    echo "{ \"command\": [\"set_property\", \
        \"playlist-pos\", $currentTrack ] }" | $SOCKETCOMMAND > /dev/null
}

setSpeedRelative() {
    speed=$(getSpeed)

    fltCheck $1
    speed=$(echo "$speed+$1\n" | bc)

    echo "{ \"command\": [\"set_property_string\", \"speed\", \
        \"$speed\" ] }" | $SOCKETCOMMAND > /dev/null
}

setSpeedAbsolute() {
    fltCheck $1
    speed=$1

    echo "{ \"command\": [\"set_property_string\", \"speed\", \
        \"$speed\" ] }" | $SOCKETCOMMAND > /dev/null
}

alwaysPlay() {
    echo "{ \"command\": [\"set_property\", \"pause\", false ] }" | \
        $SOCKETCOMMAND > /dev/null
}

togglePause() {
    status=$(getPauseStatus)
    test $status = "playing" && status="true" || status="false"
    echo "{ \"command\": [\"set_property\", \"pause\", $status ] }" | $SOCKETCOMMAND > /dev/null
}

toggleLoopFile() {
    loopFile=$(getLoopStatus loop-file)

    test $loopFile = "no" && loopFile="inf" || loopFile="no"

    echo "{ \"command\": [\"set_property_string\", \
        \"loop-file\", \"$loopFile\" ] }" | $SOCKETCOMMAND > /dev/null
}

toggleLoopPlaylist() {
    loopPlaylist=$(getLoopStatus loop)

    test $loopPlaylist = "no" && loopPlaylist="inf" || loopPlaylist="no"

    echo "{ \"command\": [\"set_property_string\", \
        \"loop\", \"$loopPlaylist\" ] }" | $SOCKETCOMMAND > /dev/null
}

    # Time Functions
###############################################################################

elaspedTime() {
    time=$(getValue playback-time)
    formatTime $time
}

trackLength() {
    duration=$(getDuration)
    formatTime $duration
}

# format seconds into HH:MM:SS format
formatTime() {
    rawSeconds=$1
    seconds=$((rawSeconds % 60))
    minutes=$((rawSeconds / 60))
    hours=$((minutes / 60))

    test $seconds -lt 10 && seconds="0$seconds"
    test $minutes -ge 60 && minutes=$((minutes - hours*60))
    test $minutes -lt 10 && minutes="0$minutes"
    test $hours -lt 10 && hours="0$hours"

    echo "$hours:$minutes:$seconds"
}

    # Misc. Functions
###############################################################################

color() {
    f0='[30m'; f1='[31m'; f2='[32m'; f3='[33m'
    f4='[34m'; f5='[35m'; f6='[36m'; f7='[37m'
    b0='[40m'; b1='[41m'; b2='[42m'; b4='[44m'
    b4='[44m'; b5='[45m'; b6='[46m'; b7='[47m'
    B='[1m'; R='[0m'; I='[7m'

    cat << COLOR
$f0$B$I$b2█$I$b7 $@ $I$b7$R
COLOR
}

intCheck() {
    test $1 -ne 0 2> /dev/null
    test $? -ne 2 || {
         echo "$1 is not an integer." >&2
         exit 1
    }
}

fltCheck() {
    testDecimal=$(echo "$1\n" | bc)
    test $testDecimal -eq 0 2> /dev/null && {
        echo "$1 is not a number." >&2
        exit 1
    }
}

validateSocket() {
    test "$PLAYFLAG" != "true" && {
        # test if socket exists
        test -S $SOCKET || {
            echo "$SOCKET does not exist." >&2
            exit 2
        }

        # test if socket is open
        status="$(getPauseStatus)"
        test "$status" = "N/A" && {
            echo "$SOCKET is not currently open." >&2
            exit 3
        }
    } || {
        return
    }
}

# kill all instances of mpv if given socket process is not found by pgrep.
killProcess() {
    pgrep "$SOCKET" -f 2>&1 > /dev/null && {
        pkill "$SOCKET" -f 2>&1 > /dev/null
    } || {
        pkill "mpv"
    }
}

# formats and echos according to $FORMATSTRING
formatPrint() {
    echo $FORMATSTRING | sed "
    s/%year%/$(getMetadata date)/;
    s/%genre%/$(getMetadata genre)/;
    s/%title%/$(getMetadata title)/;
    s/%album%/$(getMetadata album)/;
    s/%artist%/$(getMetadata artist)/;
    s/%albumartist%/$(getMetadata album_artist)/;
    s/%speed%/$(getSpeed)/;
    s/%time%/$(elaspedTime)/;
    s/%length%/$(trackLength)/;
    s/%status%/$(getPauseStatus)/;
    s/%percentage%/$(getPercentage)/;
    s/%filename%/$(getMediaFilename)/;
    s/%repeat%/$(getLoopStatus loop)/;
    s/%single%/$(getLoopStatus loop-file)/;
    s/%playlistlength%/$(getValue playlist-count)/;
    s/%frame%/$(getFrameNumber)/;
    s/%position%/$(($(getValue playlist-pos) + 1))/;"
}

# print default status of mpv instance
printDefaultStatus() {
    FORMATSTRING="%title% | %artist% | %album% | %position%"
    formatPrint
}

main() {
    # get the user defined socket before anything else
    for arg in "$@"; do

        test "$SOCKETFLAG" = "true" && {
            SOCKET=$arg
            SOCKETFLAG=false
        }

        case "$arg" in
            -a) PLAYFLAG=true   ;;
            -q) QUIETFLAG=true  ;;
            -S) SOCKETFLAG=true ;;
            -h) usage 0         ;;
        esac
    done

    validateSocket

    for arg in "$@"; do
        case "$arg" in
            -a)
                test "$FORMATFLAG" = "true" && FORMATFLAG=false
                ;;
            -f)
                test "$APPENDFLAG" = "true" && APPENDFLAG=false
                ;;
            --) ;;
            # use ? wildcard to match all other options
            -?)
                test "$FORMATFLAG" = "true" && FORMATFLAG=false
                test "$APPENDFLAG" = "true" && APPENDFLAG=false
                ;;
        esac
        test "$FORMATFLAG" = "true" && FORMATSTRING="$FORMATSTRING $arg"
        test "$APPENDFLAG" = "true" && APPENDSTRING="$APPENDSTRING $arg"
        case "$arg" in
            -f) FORMATFLAG=true ;;
            -a) APPENDFLAG=true ;;
        esac
    done

    for arg in "$@"; do
        case "$arg" in
            -a)
                test "$QUIETFLAG" = "true" && {
                    praseAppendTracks &
                } || {
                    praseAppendTracks
                }
                exit
                ;;
            -f)
                formatPrint
                exit
                ;;
        esac
    done

    while getopts "hFqS:s:t:v:V:r:R:x:X:zpPmlLik" opt; do
        case "$opt" in
            F)  listFormat                          ;;
            s)  setTimeRelative $OPTARG             ;;
            t)  setTimeAbsolute $OPTARG             ;;
            v)  setVolumeRelative $OPTARG           ;;
            V)  setVolumeAbsolute $OPTARG           ;;
            r)  setTrackRelative $OPTARG; sleep 0.5 ;;
            R)  setTrackAbsolute $OPTARG; sleep 0.5 ;;
            x)  setSpeedRelative $OPTARG            ;;
            X)  setSpeedAbsolute $OPTARG            ;;
            p)  togglePause                         ;;
            P)  alwaysPlay                          ;;
            l)  toggleLoopFile                      ;;
            L)  toggleLoopPlaylist                  ;;
            z)  shufflePlaylist                     ;;
            i)  getPlaylist; QUIETFLAG=true         ;;
            k)  killProcess; QUIETFLAG=true         ;;
            \?) echo; usage 1                       ;;
        esac
    done

    test "$QUIETFLAG" = "true" || printDefaultStatus
}

main "$@"
