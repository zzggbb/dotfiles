#!/bin/sh

dir="$1"
out="$2"

usage(){
  echo "usage: $(basename $0) <dir> <combined_name>"
  exit 1
}

test -z "$dir" && usage
test -z "$out" && usage

for pdf in "${dir}"/*.pdf; do
  mutool clean -D -d -c -s "$pdf" "$pdf.clean.pdf" 2>&1 >/dev/null
  echo "cleaned $pdf"
done

pdfunite "${dir}"/*.clean.pdf ${out}.pdf

rm -r "${dir}"
