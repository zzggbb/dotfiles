#!/bin/python3

# standard library
import sys
import time
import socket
import shutil
import subprocess

# 3rd party packages
import click

BLACKBOX_MAC_ADDRESS = '54:04:A6:41:8E:18'

BLACKBOX_LOCAL = '192.168.1.2'
ROUTER_LOCAL = '192.168.1.1'
REMOTE_DUCKDNS = 'hifiboombox.duckdns.org'
REMOTE_ASUS = 'hifiboombox.asuscomm.com'

HOSTS = {
  'blackbox': [BLACKBOX_LOCAL, REMOTE_DUCKDNS, REMOTE_ASUS],
  'router':   [ROUTER_LOCAL, REMOTE_DUCKDNS, REMOTE_ASUS],
}

def die(message):
  print(message, file=sys.stderr)
  sys.exit(1)

def ensure_packages():
  packages = ['netcat', 'ping', 'ssh']
  for pkg in packages:
    if shutil.which(pkg):
      continue
    else:
      die(f"ERROR: package {pkg} is not installed")

def check_open(host, port):
  command = f'netcat -w 5 -z {host} {port}'
  process = subprocess.run(command, shell=True, capture_output=True)
  is_open = process.returncode == 0
  #print(f"{host}:{port} is {'open' if is_open else 'closed'}")
  return is_open

def check_pingable(host):
  command = f'ping -W 1 -c1 {host}'
  process = subprocess.run(command, shell=True, capture_output=True)
  is_pingable = process.returncode == 0
  #print(f"{host} is {'pingable' if is_pingable else 'not pingable'}")
  return is_pingable

def get_first_open_host(hosts, port):
  for host in hosts:
    if check_open(host, port):
      return host
  return None

def get_first_pingable_host(hosts):
  for host in hosts:
    if check_pingable(host):
      return host
  return None

def run_remote_command(user, host, command, port=22):
  command = f"ssh {user}@{host} -p {port} -o StrictHostKeyChecking=no {command}"
  return subprocess.run(command, shell=True, capture_output=True)

def router_neighbors():
  user = 'deity'
  host = get_first_pingable_host(HOSTS['router'])
  if host is None:
    return None

  command = 'ip neigh'
  p = run_remote_command(user, host, command, port=2222)
  out = dict()
  for line in p.stdout.decode('utf-8').splitlines():
    items = line.split()
    out[items[0]] = items[-1]
  return out

def pingable_from_router(wait=1):
  user = 'deity'
  host = get_first_pingable_host(HOSTS['router'])
  if host is None:
    return None

  command = f'ping -W {wait} -c 1 {BLACKBOX_LOCAL}'
  p = run_remote_command(user, host, command, port=2222)
  return p.returncode == 0

@click.group()
def cli():
  pass

@cli.command(help='Put blackbox to sleep')
def sleep():
  if get_first_pingable_host(HOSTS['router']) is None:
    die("router is unreachable")

  if not pingable_from_router():
    die("blackbox is already asleep (or no connection between blackbox and the router...)")

  hosts = HOSTS['blackbox']
  user = 'root'
  port = 22
  host = get_first_open_host(hosts, port)
  if host is None:
    die("blackbox is already asleep! (or no connection between your device and the router...)")

  command = 'systemctl suspend'
  p = run_remote_command(user, host, command)
  print(f"running '{command}' on {user}@{host}")
  if p.returncode != 0:
    die(f"running '{command}' on {user}@{host} failed... please investigate!")

  if not pingable_from_router():
    print("successfully put blackbox to sleep")

@cli.command(help="Wake up blackbox")
def wake():
  if get_first_pingable_host(HOSTS['router']) is None:
    die("no connection between your device and the router")

  if pingable_from_router():
    die("blackbox is already awake")

  hosts = HOSTS['router']
  user = 'deity'
  port = 2222
  host = get_first_open_host(hosts, port)
  if host is None:
    die(f"no connection between your device and router:{port}")

  command = f'ether-wake -i br0 {BLACKBOX_MAC_ADDRESS}'
  p = run_remote_command(user, host, command, port=port)
  print(f"running '{command}' on {user}@{host}:{port}")
  t1 = time.time()
  if p.returncode != 0:
    die(f"running '{command}' on {user}@{host}:{port} failed... please investigate!")

  if get_first_open_host(HOSTS['blackbox'], 22):
    t2 = time.time()
    print(f"successfully woke up blackbox after {t2-t1:.1f} seconds")

if __name__ == '__main__':
  ensure_packages()
  cli()
