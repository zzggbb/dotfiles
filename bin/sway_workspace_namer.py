# must use bindsym $mod+<number> workspace number <number>

import json
from i3ipc import Connection, Event

EVENTS = [
  Event.WINDOW_TITLE,
  Event.WINDOW_NEW,
  Event.WINDOW_CLOSE,
  Event.WINDOW_MOVE,
]

title_adjustments = {
  'Google-chrome': 'chrome',
  'Spotify': 'spotify',
  'com.github.iwalton3.jellyfin-media-player': 'jellyfin',
  'org.telegram.desktop': 'telegram',
  'com.transmissionbt.transmission_66308_5508065': 'transmission-gtk',
  'org.qbittorrent.qBittorrent': 'qbtorrent',
  'io.gitlab.news_flash.NewsFlash': 'newsflash',
  'calibre-ebook-viewer': 'calibre',
  'Alacritty': 'alacritty',
  'org.pulseaudio.pavucontrol': 'pavucontrol',
  'blueman-manager': 'bluetooth',
}

def get_window_titles(workspace):
  for i, window in enumerate(workspace):
    if window.app_id:           title = window.app_id
    elif window.window_class:   title = window.window_class
    else: continue
    if title in title_adjustments: title = title_adjustments[title]
    yield title

def event_handler(wm, event):
  tree = wm.get_tree()
  for workspace in tree.workspaces():
    window_titles = list(get_window_titles(workspace))
    workspace_title = '|'.join(window_titles) if window_titles else "none"
    wm.command(f'rename workspace number "{workspace.num}" to "{workspace.num}: {workspace_title}"')

def main():
  wm = Connection()
  for event in EVENTS:
    wm.on(event, event_handler)
  event_handler(wm, 'initialize')
  wm.main()

if __name__ == '__main__':
  main()

