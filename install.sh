#!/bin/sh
install_config_file() {
  link_prefix=$1
  link_suffix=${2#*/}
  link=$link_prefix/$link_suffix
  link_dir=$(dirname $link)
  target=$HOME/dotfiles/$2

  if test $link_prefix = "/"; then
    sudo mkdir -p $link_dir
    sudo ln -svf $target $link
  else
    mkdir -p $link_dir
    ln -svf $target $link
  fi
}

if test $EUID -eq 0; then
  echo "Must be run as non-root, exiting"
  exit
fi

find home -type f | while read path; do
  install_config_file $HOME $path
done

find root -type f | while read path; do
  install_config_file / $path
done
