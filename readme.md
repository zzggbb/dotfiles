<pre>.
├── bin
│   ├── alpha
│   ├── application_menu
│   ├── bar
│   ├── bindman
│   ├── brightness
│   ├── burncd
│   ├── casper_mode.sh
│   ├── cdp
│   ├── chgpu
│   ├── clamshell_check.sh
│   ├── colorblocks
│   ├── dots
│   ├── drone
│   ├── ds30
│   ├── duckupdate
│   ├── ep
│   ├── ethdump
│   ├── evhandle
│   ├── font
│   ├── geo
│   ├── goodnight
│   ├── laptop
│   ├── launchbar.sh
│   ├── lsbin
│   ├── lsimgur
│   ├── manual_merger.sh
│   ├── matter
│   ├── mdex
│   ├── mkproj
│   ├── mml
│   ├── mod
│   ├── mplab
│   ├── mpvc
│   ├── note
│   ├── open
│   ├── pa
│   ├── pbar
│   ├── pdfrotate
│   ├── pixlock
│   ├── play
│   ├── pop
│   ├── postshot
│   ├── pproc
│   ├── pygame_init
│   ├── raidinfo
│   ├── raw2jpg
│   ├── reader
│   ├── record
│   ├── rlm
│   ├── scrot
│   ├── sct
│   ├── sct_cycle
│   ├── search
│   ├── sine
│   ├── smtpebars
│   ├── sprunge
│   ├── sway_workspace_namer.py
│   ├── sysinfo
│   ├── tag
│   ├── toggle
│   ├── togglebar
│   ├── tor
│   ├── toxrdb
│   ├── ts
│   ├── unicode-range
│   ├── updatetz
│   ├── upload
│   ├── vmm
│   ├── vs
│   ├── wakeup
│   ├── wallpaper_shuffler
│   ├── waves
│   ├── weather
│   ├── wgm
│   ├── wid
│   ├── wpm
│   ├── xeva
│   ├── xkcd.py
│   ├── xwait
│   ├── ytp
│   ├── yts
│   └── zdo
├── .config
│   ├── dunst
│   │   └── dunstrc
│   ├── htop
│   │   └── htoprc
│   ├── i3
│   │   └── config
│   ├── i3status
│   │   └── config
│   ├── nvim
│   │   ├── autoload
│   │   │   └── plug.vim
│   │   ├── init.vim
│   │   └── plugged
│   ├── polybar
│   │   └── config
│   ├── sway
│   │   └── config
│   ├── termite
│   │   └── config
│   └── weston.ini
├── .gitignore
├── install.sh
├── Makefile
├── readme.md
├── sway.service
├── .tmux.conf
├── .vimrc
├── .xinitrc
├── .Xresources
├── .zshrc
└── .zshrc_skeleton

13 directories, 103 files</pre>
